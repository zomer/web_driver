import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import ru.stqa.selenium.factory.WebDriverPool;
import util.PropertyLoader;

import java.util.concurrent.TimeUnit;

public class TestBase {
    protected WebDriver driver;

    protected String gridHubUrl;

    protected String baseUrl;

    protected String userLogin;

    protected String userPassword;

    @BeforeClass
    public void init() {
        initProperties ();

        Capabilities capabilities = DesiredCapabilities.firefox ();

        if (!(null == gridHubUrl || "".equals (gridHubUrl))) {
            driver = WebDriverPool.DEFAULT.getDriver (gridHubUrl, capabilities);
        } else {
            driver = WebDriverPool.DEFAULT.getDriver (capabilities);
        }

        driver.manage ().timeouts ().implicitlyWait (30, TimeUnit.SECONDS);
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        if (driver != null) {
            WebDriverPool.DEFAULT.dismissAll ();
        }
    }


    private void initProperties() {
        baseUrl = PropertyLoader.loadProperty ("site.url");
        gridHubUrl = PropertyLoader.loadProperty ("grid2.hub");
        userLogin = PropertyLoader.loadProperty ("user.login");
        userPassword = PropertyLoader.loadProperty ("user.password");
    }
}
