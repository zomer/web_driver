import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class LoginTest extends TestBase {

    @Test
    public void loginTest() throws Exception {
        driver.get("https://wac.templatemonster.com/signin.html");

        WebElement loginField = driver.findElement(By.name("login"));
        loginField.clear();
        loginField.sendKeys(userLogin);

        WebElement passwordField = driver.findElement(By.name("password"));
        passwordField.clear();
        passwordField.sendKeys(userPassword);

        WebElement loginBtn = driver.findElement(By.id("login_button"));
        loginBtn.submit();
    }
}
